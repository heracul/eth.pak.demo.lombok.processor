package eth.pak.demo.lombok.processor;

import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import eth.pak.demo.lombok.annotation.EthGetter;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes({ "eth.pak.demo.lombok.annotation.EthGetter" })
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class EthProcessor2 extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        JavacProcessingEnvironment environment = (JavacProcessingEnvironment) processingEnv;

        Context context = environment.getContext();
        TreeMaker maker = TreeMaker.instance(context);
        JavacElements elemUtils = (JavacElements) processingEnv.getElementUtils();

        Class<EthGetter> clazz = EthGetter.class;
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(clazz);

        for (Element element : elements) {
            JCTree tree = elemUtils.getTree(element);
            JCTree.JCMethodDecl mDecl = (JCTree.JCMethodDecl) tree;
            injectHelloWorld(maker, elemUtils, mDecl);
        }
        return false;
    }

    private void injectHelloWorld(TreeMaker maker,JavacElements elemUtils,JCTree.JCMethodDecl mDecl) {
        maker.pos = mDecl.pos;

        List<JCTree.JCExpression> nil = List.<JCTree.JCExpression> nil();
        Name system = elemUtils.getName("System");
        Name out = elemUtils.getName("out");
        Name _println = elemUtils.getName("println");
        JCTree.JCLiteral ethGetter = maker.Literal("Hello world");

        mDecl.body = maker.Block(0,List.of(
                maker.Exec(maker.Apply(nil,
                                        maker.Select(maker.Select(
                                                                maker.Ident(system),
                                                                out
                                                    ),
                                                    _println
                                        ),
                                        List.of(ethGetter)
                        )
                ),
                mDecl.body)
        );
    }
}
