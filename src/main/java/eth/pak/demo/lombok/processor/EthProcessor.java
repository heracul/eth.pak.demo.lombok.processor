package eth.pak.demo.lombok.processor;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import eth.pak.demo.lombok.annotation.MapGetter;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Set;

@SupportedAnnotationTypes({ "eth.pak.demo.lombok.annotation.EthGetter" })
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class EthProcessor extends AbstractProcessor {

    //For output java files
    private Filer filer;
    //Printing LOG
    private Messager messager;
    //Get class members, etc.
    private Elements elements;

    //Initialization gets annotation processing related implementation class, type Log class, output java code class
    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        elements = processingEnvironment.getElementUtils();
    }
    // Supported annotation classes
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new LinkedHashSet<>();
        annotations.add(MapGetter.class.getCanonicalName());
//        annotations.add(RequestMapping.class.getCanonicalName());
        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    //This function will be called many times during compilation. Be careful not to make repeated generations. The reason for calling many times should be that the generated code will be scanned again.
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.size() == 0) {
            return false;
        }
        JavacProcessingEnvironment environment = (JavacProcessingEnvironment) processingEnv;
        Context context = environment.getContext();
        TreeMaker maker = TreeMaker.instance(context);
        JavacElements javacElements = (JavacElements) processingEnv.getElementUtils();

        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(MapGetter.class);
        if(elements == null || elements.isEmpty()) return false;

        MapGetter mapGetter;
        for (Element element : elements) {
            mapGetter = element.getAnnotation(MapGetter.class);
            if (element.getKind() != ElementKind.CLASS) {
                error("The annotation @MapGetter can only be applied on field: ", element);
            } else {
                String mapName = mapGetter.ref();
                JCTree tree = javacElements.getTree(element);

                JCTree.JCClassDecl jcClassDecl = (JCTree.JCClassDecl) tree;
                List<JCTree>members = jcClassDecl.getMembers();
                for(JCTree member : members) {
                    if(member instanceof JCTree.JCVariableDecl) {
                        JCTree.JCVariableDecl variableDecl = (JCTree.JCVariableDecl)member;
                        String fieldName = variableDecl.getName().toString();
                        if(mapName.equals(fieldName)) {
                            continue;
                        }
                        generateMethod(mapName, fieldName, variableDecl, maker, javacElements, jcClassDecl);
                    }
                }
            }
        }
        return false;
    }

    private void generateMethod(String mapName,
                                String keyName,
                                JCTree.JCVariableDecl fieldNode,
                                TreeMaker maker,
                                JavacElements elemUtils,
                                JCTree.JCClassDecl cDecl) {
//        maker.pos = mDecl.pos;

        Name commonAreaMap = elemUtils.getName(mapName);
        Name getOrDefault = elemUtils.getName("getOrDefault");
        JCTree.JCLiteral mapKey = maker.Literal(keyName);
        JCTree.JCLiteral defaultValue = maker.Literal("");
        List<JCTree.JCExpression> nil = List.<JCTree.JCExpression> nil();
        List< JCTree.JCExpression > throwsClauses = List.nil();
        JCTree.JCStatement jcStatement = maker.Return(maker.TypeCast(fieldNode.getType().type, maker.Apply(List.<JCTree.JCExpression> nil(),
                maker.Select(maker.Ident(commonAreaMap),getOrDefault),
                List.of(mapKey, defaultValue)
                ))
                                        );
        JCTree.JCModifiers jcModifiers = maker.Modifiers(Flags.PUBLIC);
        List<JCTree.JCVariableDecl> parameters = List.nil();
        JCTree.JCExpression annotationMethodDefaultValue = null;
        List<JCTree.JCTypeParameter> methodGenericParams = List.nil();
        JCTree.JCMethodDecl mdecl = maker.MethodDef(jcModifiers,
                                                    elemUtils.getName(getName(true, keyName)),
                                                    maker.Type(fieldNode.getType().type),
                                                    methodGenericParams,
                                                    parameters,
                                                    throwsClauses,
                                                    maker.Block(0, List.of(jcStatement)),
                                                    annotationMethodDefaultValue
                                    );
//                (jcModifiers, Type.PUBLIC ,maker.Block(0, List.of(jcStatement)))
        cDecl.defs = cDecl.defs.append(mdecl);
    }

    private String getName(boolean isGetter, String fieldName) {
        if(isGetter) {
            return "get".concat(firstCharToUpper(fieldName));
        } else {
            return "set".concat(firstCharToUpper(fieldName));
        }
    }

    private String firstCharToUpper(String fieldName) {
        char[] chars = fieldName.toCharArray();
        if(chars != null && chars.length > 0) {
            chars[0] = Character.toUpperCase(chars[0]);
            return new String(chars);
        } else {
            return "";
        }
    }

    private void generateClass(MapGetter mapGetter, Element element)
            throws Exception {

//        String pkg = getPackageName(element);

//        //delegate some processing to our FieldInfo class
//        FieldInfo fieldInfo = FieldInfo.get(element);
//
//        //the target interface name
//        String interfaceName = getTypeName(element);
//
//        //using our JClass to delegate most of the string appending there
//        JClass implClass = new JClass();
//        implClass.definePackage(pkg);
//        implClass.defineClass("public class ", autoImplement.as(), "implements " + interfaceName);
//
//        //nested builder class
//        JClass builder = null;
//        String builderClassName = null;
//
//        if (autoImplement.builder()) {
//            builder = new JClass();
//            builder.defineClass("public static class",
//                    builderClassName = autoImplement.as() + "Builder", null);
//        }
//
//        //adding class fields
//        implClass.addFields(fieldInfo.getFields());
//        if (builder != null) {
//            builder.addFields(fieldInfo.getFields());
//        }
//
//        //adding constructor with mandatory fields
//        implClass.addConstructor(builder == null ? "public" : "private",
//                fieldInfo.getMandatoryFields());
//        if (builder != null) {
//            builder.addConstructor("private", fieldInfo.getMandatoryFields());
//        }
//
//        //generate methods
//        for (Map.Entry<String, String> entry : fieldInfo.getFields().entrySet()) {
//            String name = entry.getKey();
//            String type = entry.getValue();
//            boolean mandatory = fieldInfo.getMandatoryFields().contains(name);
//
//            implClass.createGetterForField(name);
//
//            //if no builder generation specified then crete setters for non mandatory fields
//            if (builder == null && !mandatory) {
//                implClass.createSetterForField(name);
//            }
//
//            if (builder != null && !mandatory) {
//                builder.addMethod(new JMethod()
//                        .defineSignature("public", false, builderClassName)
//                        .name(name)
//                        .addParam(type, name)
//                        .defineBody(" this." + name + " = " + name + ";"
//                                + JClass.LINE_BREAK
//                                + " return this;"
//                        )
//                );
//            }
//        }
//
//        if (builder != null) {
//
//            //generate create() method of the Builder class
//            JMethod createMethod = new JMethod()
//                    .defineSignature("public", true, builderClassName)
//                    .name("create");
//
//
//            String paramString = "(";
//            int i = 0;
//            for (String s : fieldInfo.getMandatoryFields()) {
//                createMethod.addParam(fieldInfo.getFields().get(s), s);
//                paramString += (i != 0 ? ", " : "") + s;
//                i++;
//            }
//            paramString += ");";
//
//            createMethod.defineBody("return new " + builderClassName
//                    + paramString);
//
//            builder.addMethod(createMethod);
//
//            //generate build() method of the builder class.
//            JMethod buildMethod = new JMethod()
//                    .defineSignature("public", false, autoImplement.as())
//                    .name("build");
//            StringBuilder buildBody = new StringBuilder();
//            buildBody.append(autoImplement.as())
//                    .append(" a = new ")
//                    .append(autoImplement.as())
//                    .append(paramString)
//                    .append(JClass.LINE_BREAK);
//            for (String s : fieldInfo.getFields().keySet()) {
//                if (fieldInfo.getMandatoryFields().contains(s)) {
//                    continue;
//                }
//                buildBody.append("a.")
//                        .append(s)
//                        .append(" = ")
//                        .append(s)
//                        .append(";")
//                        .append(JClass.LINE_BREAK);
//            }
//            buildBody.append("return a;")
//                    .append(JClass.LINE_BREAK);
//            buildMethod.defineBody(buildBody.toString());
//
//            builder.addMethod(buildMethod);
//            implClass.addNestedClass(builder);
//
//        }
//        //finally generate class via Filer
//        generateClass(pkg + "." + autoImplement.as(), implClass.end());
    }

//    private String getPackageName(Element element) {
//        List<PackageElement> packageElements =
//                ElementFilter.packagesIn(Arrays.asList(element.getEnclosingElement()));
//
//        Optional<PackageElement> packageElement = packageElements.stream().findAny();
//        return packageElement.isPresent() ?
//                packageElement.get().getQualifiedName().toString() : null;
//
//    }

    private void generateClass(String qfn, String end) throws IOException {
        JavaFileObject sourceFile = processingEnv.getFiler().createSourceFile(qfn);
        Writer writer = sourceFile.openWriter();
        writer.write(end);
        writer.close();
    }

    /**
     * Checking if the class to be generated is a valid java identifier
     * Also the name should be not same as the target interface
     */
    private boolean checkIdValidity(String name, Element e) {
        boolean valid = true;
        for (int i = 0; i < name.length(); i++) {
            if (i == 0 ? !Character.isJavaIdentifierStart(name.charAt(i)) :
                    !Character.isJavaIdentifierPart(name.charAt(i))) {
                error("AutoImplement#as should be valid java " +
                        "identifier for code generation: " + name, e);
                valid = false;
            }
        }
        if (name.equals(getTypeName(e))) {
            error("AutoImplement#as should be different than the Interface name ", e);
        }
        return valid;
    }

    /**
     * Get the simple name of the TypeMirror
     */
    private static String getTypeName(Element e) {
        TypeMirror typeMirror = e.asType();
        String[] split = typeMirror.toString().split("\\.");
        return split.length > 0 ? split[split.length - 1] : null;
    }

    private void error(String msg, Element e) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg, e);
    }
}
